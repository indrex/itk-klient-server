<?php

$config = array(
  'username' => 'root',
  'password' => 'mysql',
  'database' => 'test'
  );





function redirect($location){
	header("Location: " . $location);
	exit;
}





function begin_session(){

	session_start();
}

function end_session(){
	session_destroy();
}





function connect($config)
{
	try {
		$conn = new PDO('mysql:host=localhost;dbname=' . $config['database'],
						$config['username'],
						$config['password']);

		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		return $conn;
	} catch(Exception $e) {
		return false;
	}
}

function query($query, $bindings, $conn)
{
	$stmt = $conn->prepare($query);
	$stmt->execute($bindings);
	
	return ($stmt->rowCount() > 0) ? $stmt : false;

}

function get_w_limit($table_name, $conn, $limit = 10, $user_id){ 
	
	try{
		$result = $conn->query("SELECT * FROM $table_name  WHERE user_id = $user_id ORDER BY id DESC LIMIT $limit");

		return ($result->rowCount() > 0 ) ? $result : false;
	}catch(Exception $e){
		return false;
	}
}

function get_by_id($id, $conn, $table_name){
	
	$query = query("SELECT * FROM $table_name WHERE id = :id AND user_id = :user_id LIMIT 1", 
		array(
			'id' => $id,
			'user_id' => $_SESSION['user_id']
		), $conn);

	if ($query){
		return $query->fetchAll();
	} 
}

function name_check($username, $conn){
	$checkers = query("SELECT name FROM ijogi_users WHERE name = :name", array(
		'name' => $username
	), $conn);

	if (!empty($checkers)){
		foreach ($checkers as $check) {
		
			if ($username == $check['name']){
		    	$_SESSION['user'] = $username;      
			}
		}
	}
			
}

function gate($username, $password, $conn){
	

	$users = query("SELECT id, name, pass, is_admin FROM ijogi_users WHERE name = :name AND pass = SHA1(:pass)", array(
	      'name' => $username,
	      'pass' => $password
	    ), $conn);

	    //log_auth($username, $password, $conn);
	      if (!empty($users)) { 
	        
	        foreach ($users as $user) {

	          if ($username == $user['name'] && SHA1($password) == $user['pass']){
	            $_SESSION['access'] = true;
	            $_SESSION['user'] = $username;
	            $_SESSION['user_id'] = $user['id'];

	            if ($user['is_admin'] === "Y"){
	            	$_SESSION['admin'] = true;
	            	redirect('admin.php');
	            }

	            redirect("controller.php?page=home");
	          }//if
	          
	        
	        }//foreach

	     }//if not empty

}

function register($new_user, $real_name, $new_password, $new_email, $new_sex, $conn){
	query("INSERT INTO ijogi_users(name, realname, pass, email, gender) VALUES(:name, :realname, SHA1(:pass), :email, :gender)", 
        array('name'     => $new_user,
              'realname' => $real_name,
              'pass'     => $new_password,
              'email'    => $new_email,
              'gender'   => $new_sex


        ), $conn);
}

function list_users($conn){
	try{
		$users = $conn->query("SELECT * FROM ijogi_users");

		return ($users->rowCount() > 0) ? $users : false;
	}catch(Exception $e){
		return false;
	}
}

function list_posts($conn){
	try{
		$users = $conn->query("SELECT * FROM ijogi_posts ORDER BY user_id");

		return ($users->rowCount() > 0) ? $users : false;
	}catch(Exception $e){
		return false;
	}
}

function list_feedback($conn){
	try{
		$users = $conn->query("SELECT * FROM ijogi_feedback");

		return ($users->rowCount() > 0) ? $users : false;
	}catch(Exception $e){
		return false;
	}
}

function list_dreamtips($conn){
	try{
		$users = $conn->query("SELECT * FROM ijogi_dreamtips");

		return ($users->rowCount() > 0) ? $users : false;
	}catch(Exception $e){
		return false;
	}
}

function count_posts($id, $conn){
	$posts = query("SELECT COUNT(postname) FROM ijogi_posts WHERE user_id = :user_id", array(
		'user_id' => $id
	), $conn);

	if ($posts){
		return $posts->fetchAll();
	}
	
}

function get_post_date($id, $user_id, $conn){
	$date = query("SELECT post_date FROM ijogi_posts WHERE id = :id AND user_id = :user_id", array(
		'id'      => $id,
		'user_id' => $user_id
	), $conn);

	if ($date){
		return $date->fetchAll();
	}
}

function delete_post($id, $conn){
	try {
		query("DELETE FROM ijogi_posts WHERE id = :id", array(
			'id' => $id
		), $conn);
	} catch(Exception $e){
		return false;
	}	
		

}

function delete_improve($id, $user_id, $conn){
	try{
		query("DELETE FROM ijogi_posts WHERE id = :id AND user_id = :user_id", array(
			'id' 	  => $id,
			'user_id' => $user_id
		), $conn);
	}catch(Exception $e){
		return false;
	}
}

function update_post($desc, $cont, $id, $conn){
	try {
		query("UPDATE ijogi_posts SET postdesc = :postdesc, postcont = :postcont WHERE id = :id", 
			array(
				'postdesc' => $desc,
				'postcont' => $cont,
				'id'	   => $id
			), $conn);
	} catch(Exception $e){
		return false;
	}
}


function last_post($user_id, $conn){
	$latest_post = query("SELECT post_date FROM ijogi_posts WHERE user_id = :user_id ORDER BY post_date DESC LIMIT 1", 
		array(
			'user_id' => $user_id
		), $conn);

	if ($latest_post){
		return $latest_post->fetchAll();
	}
}

function find_user_by_id($id, $conn){
	
	$user = query("SELECT name FROM ijogi_users WHERE id = :id", array(
		'id' => $id
	), $conn);
	
	if ($user){
		return $user->fetchAll();
	}		
}

function true_user($user_id, $conn){
	$users = find_user_by_id($user_id, $conn);

		if (!empty($users)){

			echo $users[0]['name'];
		}
}

function add_tip($tip, $conn){
	try {
		query("INSERT INTO ijogi_dreamtips(tip) VALUES(:tip)", array(
 	 		'tip' => $tip
 	 	), $conn);

	} catch(Exception $e){
		return false;
	}
}

function member_since($id, $conn){
		$m_since = query("SELECT date_created FROM ijogi_users WHERE id = :id", 
		array(
			'id' => $id
		), $conn);

		if ($m_since){
			return $m_since->fetchAll();
		}
}

function add_journey($title, $desc, $cont, $user_id, $conn){
	try {
		query("INSERT INTO ijogi_posts(postname, postdesc, postcont, user_id) VALUES(:postname, :postdesc, :postcont, :user_id)" , 
          array('postname' => $title, 
                'postdesc' => $desc,
                'postcont' => $cont,
                'user_id'  => $user_id
          ), $conn);
	} catch(Exception $e){
		return false;
	}
}

function change_name($id, $name, $conn){
	try{
		query("UPDATE ijogi_users SET realname = :realname WHERE id = :id",
		array(
			'realname' => $name,
			'id' => $id
		), $conn);
	} catch(Exception $e){
		return false;
	}
	
}

function get_user_info($id, $conn){
	$userinf = query("SELECT realname, email, gender FROM ijogi_users WHERE id = :id", array(
                'id' => $id
     ), $conn);

	if ($userinf){
		return $userinf->fetchAll();
	}
}

function check_post_id($user_id, $post_id, $conn){
	try{
		query("SELECT id, user_id FROM ijogi_posts WHERE user_id = :user_id", 
		array(
			'user_id' => $user_id
		), $conn);
	} catch(Exception $e){
		return false;
	}
	
}

function delete_tips($id, $conn){
	try {
		query("DELETE FROM ijogi_dreamtips WHERE id = :id", array(
			'id' => $id
		), $conn);
	} catch(Exception $e){
		return false;
	}	
}

function delete_users($id, $conn){
	try {
		query("DELETE FROM ijogi_users WHERE id = :id", array(
			'id' => $id
		), $conn);
	} catch(Exception $e){
		return false;
	}	
}

function post_feedback($comment, $user_id, $conn){
	try {
		 query("INSERT INTO ijogi_feedback(comment, user_id) VALUES(:comment, :user_id)", array(
            'comment' => $comment,
            'user_id' => $user_id
          ), $conn);
	} catch (Exception $e){
		return false;
	}
}
