<?php 
  require_once("functions.php");


  $conn = connect($config);

  if (!$conn) die("We got a problem");

  begin_session();

  $_SESSION['access'] = false;
  $_SESSION['admin'] = false;


  $min = 5;
  $max = 49;

  if (isset($_POST["submit"])){


    
    $errors = array();

    

    $username = $_POST['username'];
    $password = $_POST['password'];

    name_check($username, $conn);
    

    gate($username, $password, $conn);
    
 
    if (!isset($username) || $username === "") {
       $errors['username_val'] = "Username can't be blank.";
     }
    
    elseif ($username !== $_SESSION['user']){
      $errors['username_incorrext'] = "Invalid username.";
    }
    

    elseif (!isset($password) || $password === "") {
       $errors['password_val'] = "Password can't be blank.";
    } 
    
    elseif ($password){
      $errors['password_incorrect'] = "Incorrect password.";
    }
    
    else {
      $username = $_POST["username"];
      $errors['userinfo_x'] = "Login error. Check your username and password.";
    }


  } else {
    $username = "";
    $message = "Feel free to log in";
  }

  

  require_once("views/indexheader.php");

  require_once("views/indexmain.php");

  require_once("views/indexfooter.php");
?>



