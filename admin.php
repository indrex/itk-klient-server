<?php 
	
    require_once("functions.php");

      $conn = connect($config);

  		if (!$conn) die("We got a problem");

 	 begin_session();

 	 require_once("views/header.php");

 	 if (isset($_POST['tipper'])){
 	 	$tip = $_POST['dreamtip'];

 	 	$errors = array();

 	 	if (!isset($tip) || empty($tip)){

 	 		$errors['tip'] = "Can't add an empty tip.";

 	 	} else {
 	 		add_tip($tip, $conn);

 	 		$message = "Tip added.";

 	 	}

 	 	
 	 }


	require_once("views/adminview.php");
											

	require_once("views/footer.php");
?>


