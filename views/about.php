      <?php 


        if (isset($_POST['feedback'])){
          $feedback = $_POST['feedtext'];

            $errors = array();

          if (!isset($feedback) || empty($feedback)){
              $errors['feedback'] = "What's on your mind?";
          } else {

            post_feedback($feedback, $_SESSION['user_id'], $conn);         

            $message = "Thank you for your feedback :)";

          }


        }


      ?>



      <section class="container main">
        <h1>About</h1>
        
        <hr>

        <?php if (!empty($errors)): ?>
          <?php foreach($errors as $error): ?>
            <ul>
              <li><?php echo $error; ?></li>
            </ul>
          <?php endforeach; ?>
        <?php endif; ?>

        <?php if (isset($message)): ?>
          <h4><?php echo $message; ?></h4>
        <?php endif;?>

        <div class="row">
          
          <div class="col-md-8">
            <h2 class="text-info">The dream team is composed of secret wizards who work at night to bring clarity to your dreamscape.</h2>
            <br>
            <br>
            <form action="controller.php?page=about" method="POST">
              <label for="write-to-team">Tell us what's on your mind:</label>
              <textarea id="write-to-team" class="form-control" name="feedtext" placeholder="Write us about problems, suggestions and general feedback." rows="4" autofocus></textarea>
              <br>
              <input type="submit" class="btn btn-info" value="Send" name="feedback">
            </form>
          </div><!-- dream team info -->
        
        </div>
        
        

      </section>