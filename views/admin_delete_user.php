<?php 

	$id = (int)$_GET['id'];

	delete_users($id, $conn);

	$_SESSION['notice'] = "Deleted user ID {$id}";

	redirect("admin.php#userTable");

?>