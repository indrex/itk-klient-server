<?php 

	$id = (int)$_GET['id'];

	delete_tips($id, $conn);

	$_SESSION['notice'] = "Deleted tip ID {$id}";

	redirect("admin.php#postsTable");

?>