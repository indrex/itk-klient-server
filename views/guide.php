<section class="container main">
        <h1>Dream Guide</h1>

        <hr>

        <div class="row">
          <div class="col-md-6" id="dream-tech">
          <h2 class="text-info">Useful techniques</h2>
            
            <?php $tips = list_dreamtips($conn);?>

            <ul>
              <?php if (!empty($tips)): ?>
                <?php foreach($tips as $tip): ?>
                  <li><?php echo $tip['tip']; ?></li>
                <?php endforeach;?>
              <?php endif; ?> 
            </ul>

        </div>

      </div>
        
        
</section>