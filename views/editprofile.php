<?php 
  

  if (isset($_POST['usrupdate'])){

      $errors = array();

      $name = $_POST['changename'];

    if (!isset($name) || empty($name)){
      $errors['name'] = "Please insert a valid name.";
    }else {
      change_name($_SESSION['user_id'], $name, $conn);

      redirect("controller.php?page=home");
    }
     
  }


  $name = get_user_info($_SESSION['user_id'], $conn);


?>


<section class="container main">
       <div class="row">
        <h1>Edit profile</h1>

        <?php if(!empty($errors)): ?>
          <ul>
          <?php foreach($errors as $error): ?>
            <li class="text-danger"><?php echo $error; ?></li>
          <?php endforeach; ?>
          </ul>
        <?php endif; ?>

        <hr>
       </div>

       <div class="row" id="profile-view">
         <div class="col-md-8 content-box">
            <!-- <img src="images/e0d179fc5739a20308331b432e4f3a51.jpeg" alt="Darthy" class="img-responsive img-rounded pull-right">-->
            <h3><?php echo htmlspecialchars($name[0]['realname']);?>:</h3>

            <hr>

            <form action="controller.php?page=editprofile" method="POST">

              <!--<label for="changePhoto" class="text-info">Change profile image</label>
              <input type="file" class="btn" name="pic">-->

              <label for="changeName" class="text-info">Change name:</label>
              <input type="text" placeholder="Change your name" class="form-control" id="changeName" name="changename">

              <br>

              <input type="submit" value="Change" class="btn btn-success" name="usrupdate">

            </form>
            
         </div>
        </div>
    </section>  
