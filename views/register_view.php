<section class="col-md-4 col-md-offset-4 createUser" id="register">
        <div class="col-md-8 col-md-offset-2">
          <form action="register.php" method="POST">
            <div class="form-group">

              <h2>Create a new user</h2>

              <?php if (!empty($create_errors)): ?>
                <?php foreach ($create_errors as $type => $value): ?>
                  <ul>  
                    <li class="text-danger"><?php echo $value; ?></li>
                  </ul>
                <?php endforeach; ?>
              <?php endif; ?>
              
              <!-- NB! Serveri poolne valideerimine, aga ilma js-ita ei kuva nagunii seda. Must refactor, rethink and redeploy-->
              <hr>
              
              <label for="newname">Enter a new username:</label>
                <input type="text" name="new_user" class="form-control" id="newname"  value="<?php if (isset($new_user)) echo htmlspecialchars($new_user); ?>" placeholder="Pick an username" autofocus required>

                <br>

              <label for="realname">Enter real name:</label>
                <input type="text" name="new_realname" class="form-control" id="realname" placeholder="Enter your birthname" value="<?php if (isset($real_name)) echo htmlspecialchars($real_name); ?>" required>

                <br>

              <label for="newpass">Enter password:</label>
                <input type="password" name="new_password" class="form-control" id="newpass" placeholder="Type a password" required>

                <br>

              <label for="newpasschk">Retype password:</label>
                <input type="password" name="new_password_check" class="form-control" id="newpasschk" placeholder="Retype password" required>

                <br>

              <label for="newemail">Email:</label>
                <input type="email" name="new_email" class="form-control" id="newemail" placeholder="Enter a valid email address" value="<?php if (isset($new_email)) echo htmlspecialchars($new_email); ?>" required>

                <br>

              <label>
              <input type="radio" name="new_sex" value="M" required>Male</label>
                <br>

              <label>  
              <input type="radio" name="new_sex" value="F" required>Female</label>
                <br>
                <br>

              <input type="submit" name="submit_new" value="Create User" class="btn btn-success">
              <a href="index.php" class="btn btn-warning">Back</a>

            </div>
          </form>
        </div>
      </section>    