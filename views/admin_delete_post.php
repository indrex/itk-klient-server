<?php 

	$id = (int)$_GET['id'];

	delete_post($id, $conn);

	$_SESSION['notice'] = "Deleted post ID {$id}";

	redirect("admin.php#postsTable");

?>