<?php if ($_SESSION['admin']): ?>

	<section class="container main">
		  	<h1>Overwatch</h1>

			<hr>

		<div class="row">
			<ul class="nav nav-tabs">
  				<li class="active"><a href="#userTable" data-toggle="tab">Users Table</a></li>
  				<li><a href="#postsTable" data-toggle="tab">Posts Table</a></li>
  				<li><a href="#feedbackTable" data-toggle="tab">Feedback Table</a></li>
  				<li><a href="#addTip" data-toggle="tab">Add a new dream tip</a></li>
  				<li><a href="#tipsTable" data-toggle="tab">Tips table</a></li>
			</ul>

			<?php if (isset($_SESSION['notice'])): ?>
				<br>
				<?php echo $_SESSION['notice']; 
					unset($_SESSION['notice']);
				?>
			<?php endif; ?>

			<div class="tab-content">
  				<div class="tab-pane fade in active" id="userTable">
  					
  					<div class="col-md-12">
						
						<h2 class="text-info">Users</h2>

						<hr>

						<?php $names = list_users($conn); ?>

							<?php if (isset($message)): ?>
								<p><?php echo $message; ?></p>
							<?php endif; ?>

						<?php if (!empty($errors)): ?>
							<?php foreach($errors as $error): ?>
								<ul>
									<li><?php echo $error; ?></li>
								</ul>
							<?php endforeach; ?>
						<?php endif; ?>

						<table class="table table-bordered sortable">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Realname</th>
									<th>Email</th>
									<th>Gender</th>
									<th>User created</th>
									<th>Admin status</th>
								</tr>
							</thead>	
							<tbody>	
								<?php if (!empty($names)): ?>	
									<?php foreach ($names as $name): ?>
									<tr>
										<td><?php echo htmlspecialchars($name['id']); ?></td>
										<td><?php echo htmlspecialchars($name['name']); ?></td>
										<td><?php echo htmlspecialchars($name['realname']); ?></td>
										<td><?php echo htmlspecialchars($name['email']); ?> </td>
										<td><?php echo htmlspecialchars($name['gender']); ?></td>
										<td><?php echo htmlspecialchars($name['date_created']); ?></td>
										<td><?php echo htmlspecialchars($name['is_admin']); ?></td>
									<?php if ($name['is_admin'] == 'N'): ?>
										<td><a href="controller.php?page=admin_delete_user&id=<?php echo $name['id']; ?>" class="btn btn-danger">Delete</a></td>
									<?php endif; ?>
									</tr>
									<?php endforeach;?>
								<?php endif; ?>
							</tbody>
						</table>

				
					</div><!-- list users -->
  				</div><!-- first tab -->
  					
  				<div class="tab-pane fade" id="postsTable">
  					
  					<div class="col-md-12">

  					<h2 class="text-info">Posts</h2>

  						<hr>

						<?php $posts = list_posts($conn); ?>

						<table class="table table-bordered">
							<tr>
								<th>ID</th>
								<th>Post name</th>
								<th>Description</th>
								<th>Content</th>
								<th>Post date</th>
								<th>User</th>
							</tr>

							<?php if (!empty($posts)): ?>
								<?php foreach ($posts as $post): ?>
								<tr>
									<td><?php echo $post['id']; ?></td>
									<td><?php echo $post['postname']; ?></td>
									<td><?php echo $post['postdesc']; ?></td>
									<td><?php echo $post['postcont']; ?></td>
									<td><?php echo $post['post_date']; ?></td>
									<td>
										<?php 
											true_user($post['user_id'], $conn);
											
									 	?>
									 </td>
									 <td><a href="controller.php?page=admin_delete_post&id=<?php echo $post['id']; ?>" class="btn btn-danger">Delete</a></td>	
								</tr>
								<?php endforeach;?>
							<?php endif; ?>
						</table>
					</div><!-- list posts -->
  				</div><!-- second tab -->
  				
  				<div class="tab-pane fade" id="feedbackTable">
  					
  					<div class="col-md-12">

  					<h2 class="text-info">Feedback</h2>
  						<hr>
						<?php $feedback = list_feedback($conn);?>

						<table class="table table-bordered">
							<tr>
								<th>ID</th>
								<th>Feedback comment</th>
								<th>User</th>
							</tr>

							<?php if (!empty($feedback)): ?>
								<?php foreach($feedback as $item): ?>
									<tr>
										<td><?php echo $item['id']; ?></td>
										<td><?php echo $item['comment']; ?></td>
										<td>
											<?php 
												true_user($item['user_id'], $conn);
											?>
										</td>
									</tr>
								<?php endforeach; ?>
							<?php endif;?>
						</table>
					</div>
  				</div><!-- third tab -->

  				<div class="tab-pane fade" id="addTip">
  					<div class="col-md-12">
  						<div class="form-group">
  							<form action="admin.php" method="POST">
								<h2 class="text-info">Add a new Dream Tip</h2>

							
								
								<br>

								<label for="dreamtip">Add a new dreamtip</label>

								<br>
								
								<textarea name="dreamtip" id="dreamtip" cols="70" rows="10" required></textarea>
								
								<br>
								<br>

								<input type="submit" class="btn btn-info" name="tipper" value="Add">

  							</form>
  						</div><!-- form group -->
  					</div><!-- col md 12 -->
  					

  				</div><!-- add tip -->

  				<div class="tab-pane fade" id="tipsTable">
  					<div class="col-md-6">
						<h2 class="text-info">Dream tips</h2>


  						<?php $tips = list_dreamtips($conn);?>
							<table class="table table-bordered">
					              <?php if (!empty($tips)): ?>
					                <?php foreach($tips as $tip): ?>
					                  <tr>
					                  	<td><?php echo $tip['tip']; ?></td>
					                  	<td><a href="controller.php?page=admin_delete_tip&id=<?php echo $tip['id']; ?>" class="btn btn-danger">Delete</a></td>	
					                  </tr>
					                <?php endforeach;?>
					              <?php endif; ?> 
					        </table>
	  					
  					</div>
  				</div>	
			
			</div><!-- tab content -->
			
			

			
		
			
		</div><!-- row -->
	</section>


<?php else:?>
	<?php redirect("controller.php?page=home");?>
<?php endif; ?>