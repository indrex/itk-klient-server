<?php 
    
    $user = get_user_info($_SESSION['user_id'], $conn);
              
    $post_count = count_posts($_SESSION['user_id'], $conn);
                
    $latest_post = last_post($_SESSION['user_id'], $conn);

    $m_since = member_since($_SESSION['user_id'], $conn);

    $dreams = get_w_limit('ijogi_posts', $conn, 10, $_SESSION['user_id']);


?>


<section class="container main">
       <div class="row">
        <h1>Welcome dreamer</h1>

        <hr>
       </div>

       <div class="row" id="profile-view">
         <div class="col-md-7 content-box" id="user-info">

            <!--<img src="images/e0d179fc5739a20308331b432e4f3a51.jpeg" alt="Darthy" class="img-responsive img-rounded pull-right">-->
            <h3><?php echo $_SESSION['user'];?>'s profile:</h3>
            <br>
            <br>
            <a href="?page=editprofile" class="btn btn-success">Edit Profile</a>
            <a href="?page=addjourney" class="btn btn-info">Add a new Dream</a>
            <a href="?page=journal" class="btn btn-warning">View all Dreams</a>



            <hr>

                <p class="profile-info">Real name: <?php echo $user[0]['realname']; ?></p>
                <p class="profile-info">Email: <?php echo $user[0]['email']; ?></p>
                <p class="profile-info">Sex: <?php echo $user[0]['gender']; ?></p>

            <br>


            <button class="btn btn-danger" id="showStats">Show activity statistics</button>
            
            <table class="table table-condensed table-hover stats">
              
              <tr>
                <th>Number of posts</th>
                <th>Last post</th>
                <th>Member since</th>
              </tr>

              <tr>
                <td><?php echo $post_count[0][0]; ?></td>
                <td><?php echo $latest_post[0][0]; ?></td>
                <td><?php echo $m_since[0][0]; ?></td>

              </tr>
            
              </table>

         </div>
          <aside class="col-md-4 content-box" id="dreamlog">


            <h3>Latest dreams</h3>
            <hr>
            <ol>
              <?php if (empty($dreams)): ?>
                <p>No dreams to display</p>
              <?php endif; ?>

              <?php if (!empty($dreams)): ?>
                <?php foreach ($dreams as $dream): ?>
                  <li><a href="?page=dream&id=<?php echo $dream['id']; ?>" alt="nowhere"><?php echo "{$dream['postname']}";?></a></li>
                <?php endforeach;?>
              <?php endif; ?>
            </ol>
            <img src="images/300px-Kanizsa_triangle.svg.png" alt="triangle" class="img-responsive">
          </aside>
        </div>
</section>  