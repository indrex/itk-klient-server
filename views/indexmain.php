    <section class="col-md-4 col-md-offset-4 landingForm"  id="login">
        <h1>Welcome to Dream Journal</h1>
        <p>A site dedicated to higher awareness of ones subconcious mind</p>
        
        <hr>
        <div class="col-md-8 col-md-offset-2">
          <form action="index.php" method="POST">
            <div class="form-group">

              <?php if(isset($_SESSION['reg_success'])): ?>
                <h3 class="text-success"><?php echo $_SESSION['reg_success']; 
                    unset($_SESSION['reg_success']);?>
                </h3>
              <?php endif;?>

              <?php if (!empty($errors)): ?>
                <?php foreach ($errors as $type => $value): ?>
                  <ul>  
                    <li class="text-danger"><?php echo $value; ?></li>
                  </ul>
                <?php endforeach; ?>
              <?php endif; ?>
              <br>

              <label for="userNameLanding">Username:</label>
              <input type="text" class="form-control" id="username" placeholder="enter your username" name="username" value="<?php if (isset($username)) echo htmlspecialchars($username); ?>" autofocus>
                  <script>
                    if (!("autofocus" in document.createElement("input"))) {
                      document.getElementById("userNameLanding").focus();
                    }
                  </script>

              <br>
              
              <label for="userPasswordLanding">Password:</label>
              <input type="password" class="form-control" id="userPasswordLanding" name="password" placeholder="enter your password">
              
              <br>
              
              <input type="submit" value="Enter" name="submit" class="btn btn-success">
              <a href="register.php" class="btn btn-warning" id="regForm">Register</a>


            </div><!-- form group -->
          </form>
        </div><!-- form wrapper -->

      </section><!-- landing form --> 

      

      <span id="pos">Dream journal is stil in development</span> 