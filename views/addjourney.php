<?php 

    if (isset($_POST["submitdream"])){
      $errors = array();

      $title = $_POST['dreamtitle'];
      $desc = $_POST['dreamdesc'];
      $cont = $_POST['dreamcontent'];

      if (empty($title)) {
        $errors['no_title'] = "Please insert a title";
      }

      if (empty($desc)) {
        $errors['no_desc'] = "Please insert a description";
      }

      if (empty($cont)) {
        $errors['no_cont'] = "Please insert dream contents";
      } else {
        
        add_journey($title, $desc, $cont, $_SESSION['user_id'], $conn);
        redirect("controller.php?page=journal");
      }
    }

?>


<section class="container main">
        <h1>Add a journey</h1>

        <hr>

              <?php if (!empty($errors)): ?>
                <?php foreach ($errors as $type => $value): ?>
                  <ul>  
                    <li class="text-danger"><?php echo $value; ?></li>
                  </ul>
                <?php endforeach; ?>
              <?php endif; ?>

        <div class="col-md-7 ">
          <form action="controller.php?page=addjourney" method="POST">
            <div class="form-group">
              <label for="dreamTitle">Dream's title:</label>
              <input type="text" name="dreamtitle" class="form-control" id="dreamTitle" placeholder="Enter a title for the dream" autofocus>

              <br>

              <label for="dreamDesc">Description:</label>
              <input type="text" name="dreamdesc" class="form-control" id="dreamDesc" placeholder="Give a short description of the dream.">

              <br>

              <!--<label for="datepicker">Select date:</label>
              <br>
              <input type="date" id="datepicker"> -->

              <br>
              
              <label for="dreamContent">Content:</label>
              <textarea class="form-control" name="dreamcontent" id="dreamContent" placeholder="Enter the content of your dream." rows="10"></textarea>

              <br>
              
              <input type="submit" name="submitdream" class="btn btn-info" value="Submit Dream" id="submitdream">
              

            </div><!-- form group-->
          </form>
        </div><!-- col md-7 -->
</section>