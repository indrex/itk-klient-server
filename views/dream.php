<?php 

	$id = (int)$_GET['id'];

	$post = get_by_id($id, $conn, "ijogi_posts");

	$dates = get_post_date($id, $_SESSION['user_id'], $conn);

	$errors = array();

	foreach ($post as $entry){
		
		$post_title = $entry['postname'];
		$post_desc = $entry['postdesc'];
		$post_cont = $entry['postcont'];

	} 

	if (isset($_POST['update'])){

		$upd_desc = $_POST['chdesc'];
		$upd_cont = $_POST['chcnt'];


		if ( (!isset($upd_desc) || empty($upd_desc)) ){
			$errors['warning'] = "Description can't be empty";
		} 
		if (!isset($upd_cont) || empty($upd_cont)){
			$errors['warning2'] = "Content can't be empty";
		}

		else {

			update_post($upd_desc, $upd_cont, $id, $conn);

			$address = "controller.php?page=dream&id=" . $id;

			redirect($address);

		}
			

	}



	if (empty($post)) {
		redirect("controller.php?page=journal");
	}



?>
	<section class="container main">
		<div class="row">

			<h1>Journal entry</h1>

			<hr>

			<div class="col-md-12">
				<ol class="breadcrumb">
	  				<li><a href="#edit" id="edit">Edit</a></li>
	  				<li><a href="#delete" id="delete">Delete</a></li>
				</ol>

				<?php if (!empty($errors)): ?>
					<ul>
						<?php foreach($errors as $error): ?>
							<li><?php echo $error; ?></li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>

				<div id="editPost" class="col-md-12">
					<form action="controller.php?page=dream&id=<?php echo htmlspecialchars($_GET[id]);?>" method="POST">
						<div class="form-group">
							<?php foreach ($post as $entry){

									$post_title = $entry['postname'];
									$post_desc = $entry['postdesc'];
									$post_cont = $entry['postcont'];

								} 

							?>
							
								<h2 class="text-info"><?php echo $post_title; ?></h2>
								
								<label for="chdesc">Change description</label>
									<br>						
									<input type="text" class="form-control" id="chdesc" name="chdesc" value="<?php echo $post_desc; ?>" required>
								

								<br>

								<label for="chcnt">Change content</label>
									<br>
									<textarea class="form-control" id="chcnt" name="chcnt" rows="10" required><?php echo $post_cont; ?></textarea> 
								


							<br>

							<input type="submit" class="btn btn-info" value="Change" name="update">
							<a class="btn btn-warning" id="backDream">Back</a>
						
						</div><!--form-group -->
							
						</form>

						<hr>
				</div><!-- editpost -->

				<div class="col-md-12" id="deletePost">
							<h4>Are you sure you want to delete post: <?php echo $post_title; ?>?</h4>

							<br>

							<a href="controller.php?page=deletejourney&id=<?php echo $id; ?>" class="btn btn-danger">Yes</a>
							<a class="btn btn-success" id="cancel">No</a>

				</div>

				
				<div id="entry" class="col-md-8">
				<?php if (!empty($post)): ?>
					<?php foreach ($post as $entry): ?>
						
						<h2 class="text-primary"><?php echo $entry['postname']; ?></h2>
							<br>
							<p><?php echo "Post created/updated on {$dates[0]['post_date']}"; ?></p>
							<br>
						<h4><em>" <?php echo $entry['postdesc']; ?> "</em></h4>
							<br>
						<p><?php echo $entry['postcont']; ?></p>

					<?php endforeach ?>
				<?php endif; ?>

					<br>

					<a href="controller.php?page=journal" class="btn btn-warning">Back to journal</a>
				</div>

				
			</div><!--col-md-12  -->
		</div><!-- row -->
	</section>