<?php 
require_once("functions.php");

begin_session();

$conn = connect($config);


if (!isset($_SESSION['access']) ||$_SESSION['access'] == false){
	redirect("index.php");
}

require_once("views/header.php");




if (!isset($_GET['page'])){
	include("views/home.php");
} else {

	switch ($_GET['page']){
		case "journal":
			include('views/' . htmlspecialchars($_GET['page']) . '.php');
			break;
		case "home":
			include('views/' . htmlspecialchars($_GET['page']) . '.php');
			break;
		case "addjourney":
			include('views/' . htmlspecialchars($_GET['page']) . '.php');
			break;
		case "guide":
			include('views/' . htmlspecialchars($_GET['page']) . '.php');
			break;
		case "about":
			include('views/' . htmlspecialchars($_GET['page']) . '.php');
			break;
		case "editprofile":
			include('views/' . htmlspecialchars($_GET['page']) . '.php');
			break;
		case "dream":
			include('views/' . htmlspecialchars($_GET['page']) . '.php');
			break;
		case "admin_delete_post" && $_SESSION['admin'] == true:
			include('views/' . htmlspecialchars($_GET['page']) . '.php');
			break;
		case "deletejourney":
			include('views/' . htmlspecialchars($_GET['page']) . '.php');
			break;
		case "admin_delete_user" && $_SESSION['admin'] == true:
			include('views/' . htmlspecialchars($_GET['page']) . '.php');
			break;
		case "admin_delete_tip" && $_SESSION['admin'] == true: 
			include('views/' . htmlspecialchars($_GET['page']) . '.php');
			break;
		default:
			include("views/404.html");
	}
}

require_once("views/footer.php");

?>