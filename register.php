<?php

	require_once("functions.php");

  $conn = connect($config);

  if (!$conn) die("We got a problem"); 


	if (isset($_POST["submit_new"])){


    $create_errors = array();

    $min = 4;
    $max = 48;

    $pass_min = 8;
    $pass_max = 40;

    $new_user = $_POST['new_user'];
    $real_name = $_POST['new_realname'];
    $new_password = $_POST['new_password'];
    $new_password_check = $_POST['new_password_check'];
    $new_email = $_POST['new_email'];
    $new_sex = $_POST['new_sex'];

    if (!isset($new_user) || $new_user === "") {
       $create_errors['username_val'] = "Insert a username";
     }
    
    if (strlen($new_user) < $min) {
      $create_errors['username_min'] = "Username value too short.";
    }
    
    if (strlen($new_user) > $max) {
      $create_errors['username_max'] = "Username value too long.";
    }

    if (!isset($real_name) || $real_name === ""){
      $create_errors['realname_val'] = "Insert real name";
    }
    
    if (strlen($real_name) < $min) {
      $create_errors['name_min'] = "Name value too short.";
    }
    
    if (strlen($real_name) > $max) {
      $create_errors['name_max'] = "Name value too long.";
    }
    
    if (!isset($new_password) || $new_password === "") {
       $create_errors['password_val'] = "Please insert a password.";
    } 
    
    if (strlen($new_password) < $pass_min) {
      $create_errors['pass_min'] = "Password value too short.";
    }
    
    if (strlen($new_password) > $pass_max) {
      $create_errors['pass_max'] = "Password value too long.";
    }
   
    if ($new_password !== $new_password_check){
      $create_errors['password_incorrect'] = "Passwords don't match.";
    }
    
    if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$new_email)){
      $create_errors['invalid_email'] = "Insert a correct email address";
    }

    if (empty($create_errors)){

      
      
     try{
        
      register($new_user, $real_name, $new_password, $new_email, $new_sex, $conn);

      $_SESSION['reg_success'] = "User created";
      
      redirect("index.php");



     }catch(Exception $e){
        $create_errors['unique'] = "Duplicate entry found";
     }
      

    }



  }

	require_once("views/indexheader.php");

	require_once("views/register_view.php");

	require_once("views/indexfooter.php");
?>