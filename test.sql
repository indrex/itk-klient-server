-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 24, 2014 at 05:18 AM
-- Server version: 5.5.37
-- PHP Version: 5.5.12-2+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `ijogi_dreamtips`
--

CREATE TABLE IF NOT EXISTS `ijogi_dreamtips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tip` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `ijogi_dreamtips`
--

INSERT INTO `ijogi_dreamtips` (`id`, `tip`) VALUES
(1, 'Close your eyes.'),
(2, 'After experiencing a dream, write it down as soon as possible'),
(3, 'Keep your dream journal close to your sleeping place'),
(4, 'If you remember something during the day, write it down as soon as possible'),
(5, 'If some object, person or a feeling makes you remember some detail about your dream, write it down also'),
(8, 'Dreaming usually starts when your body has become rested'),
(10, 'Try to spot things inside your dreams that appear as if they can only happen in a dream'),
(11, 'Before falling asleep try to focus your mind so you can spot inside a dream details that make you become aware of your dreaming state'),
(12, 'Before falling asleep try to think about what you want to dream');

-- --------------------------------------------------------

--
-- Table structure for table `ijogi_feedback`
--

CREATE TABLE IF NOT EXISTS `ijogi_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ijogi_feedback`
--

INSERT INTO `ijogi_feedback` (`id`, `comment`, `user_id`) VALUES
(1, 'I really like your site', 13),
(2, 'fff', 13),
(3, 'I''m an admin and it''s fantastic', 3),


-- --------------------------------------------------------

--
-- Table structure for table `ijogi_posts`
--

CREATE TABLE IF NOT EXISTS `ijogi_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postname` varchar(150) NOT NULL,
  `postdesc` varchar(150) NOT NULL,
  `postcont` text NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `ijogi_posts`
--

INSERT INTO `ijogi_posts` (`id`, `postname`, `postdesc`, `postcont`, `post_date`, `user_id`) VALUES
(7, 'Lucid Dream', 'Living in another reality', 'I always doubt everything after an experience like that.', '0000-00-00 00:00:00', 4),
(8, 'Hello', 'World', 'Life', '0000-00-00 00:00:00', 4),
(9, 'lala', 'lolo', 'lole\r\n', '0000-00-00 00:00:00', 4),
(10, 'It was so yolo', 'Swag', 'I really don''t like this vanity', '0000-00-00 00:00:00', 9),
(12, 'Not bad ', 'for a first time', 'It sure won''t be the last', '0000-00-00 00:00:00', 9),
(13, 'My First Dream', 'The one that makes you remember', 'Happened so long ago', '0000-00-00 00:00:00', 13),
(15, 'I better go to bed', 'Dope', 'yolo is so yesterday', '0000-00-00 00:00:00', 13),
(16, 'Yolo', 'It''s party time', 'Your bones don''t break, mine do. That''s clear. Your cells react to bacteria and viruses differently than mine. You don''t get sick, I do. That''s also clear. But for some reason, you and I react the exact same way to water. We swallow it too fast, we choke. We get some in our lungs, we drown. However unreal it may seem, we are connected, you and I. We''re on the same curve, just on opposite ends.', '0000-00-00 00:00:00', 13),
(20, 'Kassid magavad koguaeg', 'Ja tahavad sÃ¼Ã¼a, kui neil selleks tuju on', 'Tihti tahavad nad ka Ãµue minna ja pendeldavad edasi tagasi', '0000-00-00 00:00:00', 21),
(22, 'yo', 'yo yo yo', 'hey hey hey hey', '2014-05-23 11:12:16', 3),
(25, 'Dumpty Dii', 'Woopdy doo', 'Hey hey hey I saved the world again...', '2014-05-23 11:59:14', 3);

-- --------------------------------------------------------

--
-- Table structure for table `ijogi_users`
--

CREATE TABLE IF NOT EXISTS `ijogi_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `realname` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `gender` enum('M','F') NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_admin` enum('Y','N') DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `ijogi_users`
--

INSERT INTO `ijogi_users` (`id`, `name`, `realname`, `pass`, `email`, `gender`, `date_created`, `is_admin`) VALUES
(3, 'admin', 'Dream Master', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'ijogi@itcollege.ee', 'M', '0000-00-00 00:00:00', 'Y'),
(4, 'Dreamy Dreamer', 'Nemo Nautilus', 'password', 'rules@underwater.com', 'M', '0000-00-00 00:00:00', 'N'),
(8, 'a new dawn', 'truly free', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'email@mail.ee', '', '0000-00-00 00:00:00', NULL),
(9, 'yolomon', 'holomon', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'fkfkfk@kfkf.ee', 'M', '0000-00-00 00:00:00', NULL),
(10, 'one more time', 'just one fix', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'ihave@none.ee', 'M', '0000-00-00 00:00:00', NULL),
(11, 'a girly one', 'there ought to be one', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'email@email.com', 'F', '0000-00-00 00:00:00', NULL),
(13, 'Yukiro', 'Huki DUkI', 'f865b53623b121fd34ee5426c792e5c33af8c227', 'yoyo@yoyo.yo', 'F', '0000-00-00 00:00:00', 'N'),
(14, 'Sunshine', 'Happy Person', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'happy@sunny.yeah', 'F', '0000-00-00 00:00:00', 'N'),
(17, 'ldldldlldldl', 'dldldldldldlld', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'heyhey@hey.ee', 'M', '0000-00-00 00:00:00', 'N'),
(18, 'dkkdkdkdkdk', 'dkdkdkdkdkdkdk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'yolo@kolo.dolo', 'M', '0000-00-00 00:00:00', 'N'),
(19, 'hey hey hey', 'hey hey hey', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'hey@yo.hi', 'F', '0000-00-00 00:00:00', 'N'),
(20, 'dumbuser', 'Trixy Dixy Toes', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'hey@living.com', 'F', '0000-00-00 00:00:00', 'N'),
(21, 'vargenstein', 'Varg Kass', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'varg@kass.ee', 'M', '0000-00-00 00:00:00', 'N'),
(22, 'rudolfo', 'Ruts von Ruberoid', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'yolo@swag.bz', 'M', '0000-00-00 00:00:00', 'N'),
(23, 'newestuser', 'Real Name', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'real@email.ru', 'M', '2014-05-22 23:09:15', 'N'),
(24, 'Texas Red', 'A Ranger', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'ranger@arizona.com', 'M', '2014-05-24 02:10:14', 'N'),
(25, 'kafka', 'fkfkkffkk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'yolo@holo.zz', 'M', '2014-05-24 02:11:50', 'N');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ijogi_feedback`
--
ALTER TABLE `ijogi_feedback`
  ADD CONSTRAINT `ijogi_feedback_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `ijogi_users` (`id`);

--
-- Constraints for table `ijogi_posts`
--
ALTER TABLE `ijogi_posts`
  ADD CONSTRAINT `ijogi_posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `ijogi_users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
